#!/usr/bin/env python
# -*- coding: utf-8 -*-
from LunarSolarConverter import *
from GenerateIcs import add_full_day_event, add_all_events_to_cal
import datetime


def create_birthday_ics(lunarYear, lunarMonth, lunarDay, isleap, content):
    events = []
    for i in range(100):
        lunar_date = Lunar(lunarYear + i, lunarMonth, lunarDay, isleap)
        converter = LunarSolarConverter()
        solar = converter.LunarToSolar(lunar_date)
        solar_date = datetime.date(solar.solarYear, solar.solarMonth, solar.solarDay)
        print solar_date
        events.append(add_full_day_event(solar_date, content + "第" + str(i) + "岁生日"))
    st_d = add_all_events_to_cal(*events)
    with open('my.ics', 'w') as ics:
        ics.writelines(st_d)


create_birthday_ics(1990, 12, 13, False, "赵春奇")
