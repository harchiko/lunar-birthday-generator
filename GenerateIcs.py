#!/usr/bin/env python
# -*- coding: utf-8 -*-
from icalendar import Calendar, Event, vDatetime, vDate
from datetime import datetime


def add_full_day_event(date, summary):
    event = Event()
    date = vDate(date)
    event.add('DTSTAMP', vDatetime(datetime.now()))
    event.add('DTSTART;VALUE=DATE', date),
    event.add('DTEND;VALUE=DATE', date),
    event.add('SUMMARY', summary)
    event.add('UID', hash(str(date) + summary))
    return event


def add_all_events_to_cal(*events):
    cal = Calendar()
    for event in events:
        cal.add_component(event)
    return cal.to_ical()



m = add_all_events_to_cal(add_full_day_event(datetime.now(), "奶奶生日"))
with open('my.ics', 'w') as ics:
    ics.writelines(m)
